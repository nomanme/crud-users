import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from './components/pages/Home';
import About from './components/pages/About';
import Contact from './components/pages/Contact';
import Navbar from './components/layout/Navbar';
import NotFound from './components/pages/NotFound';
import AddUser from './components/users/AddUser';
import EditUser from './components/users/EditUser';
import ViewUsers from './components/users/ViewUsers';

function App() {
  return (

    <Router>
      <div className="App">
        <Navbar></Navbar>

        <Switch>
          <Route exact path="/" component={Home} ></Route>
          <Route exact path="/about" component={About}></Route>
          <Route exact path="/contact" component={Contact}></Route>
          <Route exact path="/users/add" component={AddUser}></Route>
          <Route exact path="/users/edit/:id" component={EditUser}></Route>
          <Route exact path="/users/:id" component={ViewUsers}></Route>
          <Route component={NotFound}></Route>

          <Home></Home>
          <About></About>
          <Contact></Contact>
          <NotFound></NotFound>
          <AddUser></AddUser>
          <EditUser></EditUser>
          <ViewUsers></ViewUsers>

        </Switch>

      </div>
    </Router>

  );
}

export default App;
